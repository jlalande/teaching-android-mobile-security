# Teaching Android Mobile Security

This web page contains the materials for the labs described in the paper:

```
J.-F. Lalande, V. Viet Triem Tong, P. Graux, G. Hiet, W. Mazurczyk, H. Chaoui, and P. Berthomé,
“Teaching Android Mobile Security,” in SIGCSE’19. Minneapolis, USA: ACM, 2019.
```

If you use parts of these labs for your own labs, please cite our paper in your documents.

The provided resources are:

- the [MAL](MAL-labs/) lab: Malware Reverse Engineering
    - [Exercise 1](MAL-labs/01-ransomware.md): Preparing an antidote for a ransomware
    - [Exercise 2](MAL-labs/02-spyware.md): Hacking a Spyware
    - [Exercise 3](MAL-labs/03-adware+blare.md): Observing under cover the Mobidash adware
    - [Exercise 5](MAL-labs/05-soot.md): Operate a malware as easy as ABC
    - [Exercise 6](MAL-labs/06-steal_internal.md): Steal a malware
    - [Exercise 7](MAL-labs/07-network_sniffer.md): Capturing the data leaked by a Spyware
- the [COV](COV-lab.md) lab: Developing Covert Channels
- the [CLASS](CLASS-lab/CLASS-lab.md) lab: Vulnerable Class Loader
- the [PACK](PACK-lab/PACK-lab.md) lab: Packers
- the [MALTOY](MALTOY-lab/MALTOY-lab.md) lab: Reversing an obfuscated malware



