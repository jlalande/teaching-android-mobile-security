# MATOY lab: Obfuscated Application

## Prerequisite

For this exercise, you need to use an Android emulator. 
Android SDK version 34 is recommended. 

If you are not familiar with android dynamique analysis, you should do the [introduction to repackaging and dynamique analysis](./TUTO-lab.md) before.

### Recommended tools

- [Jadx](https://github.com/skylot/jadx/releases)
- [Apktool](https://github.com/iBotPeaches/Apktool)
- [Frida](https://frida.re/)
- Any tool you think you might need

## Introduction

This is an 'open' exercise, you are free to approach it anyway you want.

In this exercise, you will analyse a toy malware developed to showcase some obfuscations techniques.
To avoid unfortunate accidents, the malware will abort if not run inside an emulator.

!!! Warning !!! Download the application [signature](./wanna_play_a_game.apk.idsig) and put it next to the application file before installing the application with `adb wanna_play_a_game.apk` (For some reason, the application will crash if not installed with the signature).

Install and run [wanna_play_a_game](./wanna_play_a_game.apk), reverse it, and answer the questions bellow. Additional remarks are welcome.

## Questions

- What behavior do you notice when running the malware?
- What permissions are used by the malware?
- Describe the obfuscation techniques you observed.
- Describe the 3 "malicious" actions the malware perform.
- Describe under which condition the malware will not perform those malicious actions.

## Hints

- The malware uses a lot of decoys, make sure you are looking at the right class.
- The malicious code is in a class called `com.game.Main` (no, not this one).
- Because a class looks like a class from the android SDK does not means it's from the android SDK.
- No seriously, are you sure you are looking at the right class?
