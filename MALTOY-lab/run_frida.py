import frida

import base64

# Callback to receive the data send by the `send` function in javascript.
def on_message(message, data):
    if message['type'] == 'send' and message['payload'].startswith("dex: "):
        with open("deciphered.dex", "wb") as file:
            file.write(base64.b64decode(message['payload'].removeprefix("dex: ")))
        print("[-] dex file saved at 'deciphered.dex'")
    elif message['type'] == 'send':
        print(f"[*] {message['payload']}")
    elif message['type'] == 'error':
        print(message["description"])
        print(message["stack"])
    else:
        print(message)

# Javascript code run in the app process.
jscode = """
Java.perform(() => {
  // Get the PathClassLoader class
  const PathClassLoader = Java.use('dalvik.system.PathClassLoader');
  // Get the constructor function
  const constructor = PathClassLoader.$init.overload('java.lang.String', 'java.lang.ClassLoader');
  // Override the constructor implementation
  constructor.implementation = function (path, class_loader) {
    // Send data back to the python client
    send('Load code from: ' + path);

    const Files = Java.use('java.nio.file.Files');
    const Path = Java.use('java.nio.file.Path');
    const StandardCopyOption = Java.use('java.nio.file.StandardCopyOption');
    const original = Path.of(path, []);
    const copy = Path.of("/data/user/0/com.example.tuto/cache/saved.dex", []);
    const option = StandardCopyOption.REPLACE_EXISTING.value;
    const options = Java.array("java.nio.file.StandardCopyOption", [option]);
    Files.copy(original, copy, options);

    //const Files = Java.use('java.nio.file.Files');
    //const Path = Java.use('java.nio.file.Path');
    const Base64 = Java.use('android.util.Base64');
    const dexfile = Path.of(path, []);
    const dex = Files.readAllBytes(dexfile);
    const b64 = Base64.encodeToString(dex, Base64.DEFAULT.value);
    send("dex: " + b64)



    // run the original implementation of the constructor
    return this.$init(path, class_loader);
  };

  const String = Java.use('java.lang.String');
  const equals = String.equals.overload('java.lang.Object');
  equals.implementation = function (other) {
      const v1 = this.toString();
      const v2 = other.toString();
      if (v1.includes("FLAG")) {
          send('intercepted "' + v1 + '" == "' + v2 + '"');
      }
      return this.equals(other);
  }
  
});
"""

# Connect to the frida server on the emulator
device = frida.get_usb_device()
# Spawn the app process
pid = device.spawn(["com.example.tuto"])
# Attach a session to the app process
session = device.attach(pid)
# Add the script to the app process
script = session.create_script(jscode)
# Define the callback to `send`
script.on('message', on_message)
# Load the script
script.load()
# Start the process
device.resume(pid)
# Frida run in another thread, that will terminate with the main thread.
# this line prevent the main thread from terminating.
input()
