# Introduction to Android repackaging and dynamic instrumentation

## Required Tools

- [Jadx](https://github.com/skylot/jadx/releases)
- [Apktool](https://github.com/iBotPeaches/Apktool)
- [Python](https://www.python.org/)
- [Frida](https://frida.re/)
- [Android Emulator](https://developer.android.com/studio/run/emulator)(Root, SDK34, cf setup)
- Other Android standard tools (cf setup)

## Setup Android Emulator and install the application

For this lab, we need a rooted image of Android, and we are targeting the SDK34 version of Android.
Other versions probably work, but Android API/security measure can change a lot and cause unexpected issues.

You can setup the emulator via android studio or the CLI. The exact image needed is `system-images;android-34;default;x86_64` (or `system-images;android-34;default;arm64-v8a` if you have an ARM CPU).

For simplicity, we will describe the CLI version. We will suppose that your Android SDK is installed at `~/Android/Sdk/` (It may not be the same for you, it depends on where you installed [`sdkmanager`](https://developer.android.com/tools/sdkmanager). You can install it with Android Studio (recommended, tools>"SDK manager", "Languages & Framework">"Android SDK">"SDK Tools" -> "Android SDK Command-line Tools (latest)", the location of the SDK is set in the field "Android SDK Location") or manually.

![Image showing the GUI interface to install sdkmanager](./images/studio_sdkmanager.png)

You should add `~/Android/Sdk/cmdline-tools/latest/bin/` to your PATH to avoid using `~/Android/Sdk/cmdline-tools/latest/bin/sdkmanager` each times.

Now you need to install the emulator, the image to use, the platform tools (to get `adb`, the debugging tool to connect to the android emulator) and the build tools (for resigning the application):

```sh
sdkmanager emulator
sdkmanager 'system-images;android-34;default;x86_64'
sdkmanager platform-tools
sdkmanager 'build-tools;34.0.0'
```

Now you should add `~/Android/Sdk/platform-tools/`, `~/Android/Sdk/emulator/` and `~/Android/Sdk/build-tools/34.0.0/` to your path (or use `~/Android/Sdk/platform-tools/adb`, `~/Android/Sdk/emulator/emulator`, `~/Android/Sdk/build-tools/34.0.0/zipalign` and `~/Android/Sdk/build-tools/34.0.0/apksigner` each time).

Now we can create the Android Virtual Device (AVD):

```sh
avdmanager create avd --name root34 --package 'system-images;android-34;default;x86_64'
# Do you wish to create a custom hardware profile? [no]
# no
```

Now you can run the emulator:

```sh
emulator -avd root34
```

If you are using wayland and having issue like `Warning: Could not find the Qt platform plugin "wayland"`, you need to set `QT_QPA_PLATFORM=xcb`: 

```sh
QT_QPA_PLATFORM=xcb emulator -avd root34
```

In another terminal, you can now install the application on the phone:

```sh
adb install tuto.apk
```

You can now have a first look at the application, called "Tutorial".

## Starting with static analysis: Jadx

Start by looking at the application with `Jadx`.

```console
$ jadx tuto.apk
INFO  - loading ...
INFO  - processing ...
INFO  - done
$ tree
.
├── tuto
│   ├── resources
│   │   ├── AndroidManifest.xml
│   │   ├── assets
│   │   │   ├── ciphered
│   │   │   └── data
│   │   ├── classes.dex
│   │   ├── META-INF
│   │   │   ├── MANIFEST.MF
│   │   │   ├── SIGNKEY.RSA
│   │   │   └── SIGNKEY.SF
│   │   └── res
│   │       ├── layout
│   │       │   └── dialog.xml
│   │       └── values
│   │           └── public.xml
│   └── sources
│       └── com
│           └── example
│               └── tuto
│                   ├── MainActivity.java
│                   ├── R.java
│                   └── Utils.java
└── tuto.apk

12 directories, 13 files
```

The first thing to do is to look at the manifest `AndroidManifest.xml`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    android:compileSdkVersion="34"
    android:compileSdkVersionCodename="14"
    package="com.example.tuto"
    platformBuildVersionCode="34"
    platformBuildVersionName="14">
    <uses-sdk
        android:minSdkVersion="28"
        android:targetSdkVersion="34"/>
    <application
        android:label="Tutorial"
        android:supportsRtl="true">
        <activity
            android:name="com.example.tuto.MainActivity"
            android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.LAUNCHER"/>
            </intent-filter>
        </activity>
    </application>
</manifest>
```

The format is described by google in the [Android Documentation](https://developer.android.com/guide/topics/manifest/manifest-intro). Here the main things to notice are that the application does not use any permission, and that the activity run when launching the app is `com.example.tuto.MainActivity` (see `android.intent.category.LAUNCHER` in the intent filter).

We can now look at it in `tuto/sources/com/example/tuto/MainActivity.java`.

For some reason, `setup()` failed to decompiled properly (?), but the interesting method is `requestString(String str)`. It create an `AlertDialog`, with a callback that read a text (`String obj = ((EditText) inflate.findViewById(R.id.entry)).getText().toString();`) and perform tests on it. We can see the expected format of the entry (15 characters long, starts with `FLAG{`, ends with `}`. More interesting, we can see that the application load code from an external file:

```java
File file = new File(MainActivity.this.getCacheDir(), "checker.dex");
file.setReadOnly();
Class loadClass = new PathClassLoader(file.getPath(), MainActivity.class.getClassLoader()).loadClass("com.example.tuto.Checker");
if (((Boolean) loadClass.getMethod("check", String.class, byte[].class).invoke(loadClass.getDeclaredConstructor(String.class, File.class).newInstance("Voix ambigue d'un coeur ", MainActivity.this.getCacheDir()), obj, "FLAG{_________}".getBytes())).booleanValue()) {
    MainActivity.this.popup("Congrat", "You got it!");
    return;
}
```

Jadx did no generated the most readable java code, we can reformat it a little:

```java
File file = new File(MainActivity.this.getCacheDir(), "checker.dex");
file.setReadOnly();
Class loadClass = new PathClassLoader(
    file.getPath(), 
    MainActivity.class.getClassLoader()
).loadClass("com.example.tuto.Checker");
Method method = loadClass.getMethod("check", String.class, byte[].class);
Object object = loadClass.getDeclaredConstructor(String.class, File.class)
    .newInstance("Voix ambigue d'un coeur ", MainActivity.this.getCacheDir());
if (((Boolean) method.invoke(
    object, 
    obj, 
    "FLAG{_________}".getBytes()
)).booleanValue()) {
    MainActivity.this.popup("Congrat", "You got it!");
    return;
}
```

We see that the class `com.example.tuto.Checker` from the file `checker.dex` (located in cache) is instantiated, and the method `check(String, byte[])` is called on the input string `obj`, and the binary value of `"Voix ambigue d'un coeur "`.

## First 'dynamique' steeps: ADB

We will use the Android Debuging Bridge to examine the emulator. We are looking for the file `checker.dex` in the cache of the application.
The cache of an application is private, so you will need root privilege to access it. Once you are root, you can then open a shell on the phone:

```console
$ adb root
restarting adbd as root
$ adb shell
emu64x:/ #
```

The location of the cache files depends on the SDK version and phone. To be sure, you can make an Android application and print the value of `getCacheDir()`. For this lab, you just need to know that the cache of an application is located at `/data/user/<userid>/<appid>/cache/`. Let's check:

```console
emu64x:/ # ls /data/user/0/com.example.tuto/cache/
checker.dex  ciphered
```

Found it! Now we can download the file on our computer with `adb pull`:

```console
emu64x:/ # exit
$ adb pull /data/user/0/com.example.tuto/cache/checker.dex
/data/user/0/com.example.tuto/cache/checker.dex: 1 file pulled, 0 skipped. 1.1 MB/s (3932 bytes in 0.004s)
```

## Repackaging: Apktool

We could now look at the file with Jadx, but for this lab, we will first transplant the contant of this file to the application using Apktool.

Contrary to Jadx, Apktool does not try to generate Java code, but instead smali code (an assembler like langage). This format is closer to the .dex format used by android and so easier to recompile.

Let's start by decompile the application:

```console
$ apktool d tuto.apk -o repackaged_tuto
...
$ tree repackaged_tuto
repackaged_tuto
├── AndroidManifest.xml
├── apktool.yml
├── assets
│   ├── ciphered
│   └── data
├── original
│   ├── AndroidManifest.xml
│   └── META-INF
│       ├── MANIFEST.MF
│       ├── SIGNKEY.RSA
│       └── SIGNKEY.SF
├── res
│   ├── layout
│   │   └── dialog.xml
│   └── values
│       ├── ids.xml
│       └── public.xml
└── smali
    └── com
        └── example
            └── tuto
                ├── MainActivity$1.smali
                ├── MainActivity.smali
                ├── R$attr.smali
                ├── R$id.smali
                ├── R$layout.smali
                ├── R.smali
                └── Utils.smali

11 directories, 18 files
```

Note: The main code of the application is compiled in files `classes.dex`, `classes2.dex`, `classes3.dex`, ... Apktool will decompile every `.dex` file it founds in the application (even when it is not used by Android directly), and put the results in `smali` for `classes.dex`, `smali_classes2` for `classes2.dex`, and `smali_path/to/file` for `path/to/file.dex`

Apktool can be peculiar sometime, and cannot disassemble a .dex file directly. We could use smali/baksmali, the tool used by apktool in the background to disassemble .dex, or just zip the .dex in a zip file and call it an application:

```console
$ cp checker.dex classes.dex
$ zip checker.apk classes.dex
$ rm classes.dex
$ apktool d checker.apk
...
$ tree checker
checker
├── apktool.yml
└── smali
    └── com
        └── example
            └── tuto
                └── Checker.smali

5 directories, 2 files 
```

We can see the class `com.example.tuto.Checker` loaded in `MainActivity` is the only class in this .dex. We can just copy it from `checker/smali/com/example/tuto/Checker.smali` to `repackaged_tuto/smali/com/example/tuto/`:

```sh
cp checker/smali/com/example/tuto/Checker.smali repackaged_tuto/smali/com/example/tuto/
```

Now we need to edit the smali code that load `Checker` to do it directly. First issue: the code is not in `repackaged_tuto/smali/com/example/tuto/MainActivity.smali`. The code we are looking for was in a callback, meaning in java, a different object, with a different class. The class is anonymous, so its name is derived from the defining class. Here the defining class is `MainActivity`, and the name of the anonymous class is `MainActivity$1`.

In `repackaged_tuto/smali/com/example/tuto/MainActivity$1.smali`, we find the method `onClick(Landroid/content/DialogInterface;I)V` (`void onClick(DialogInterface, int)` in java).

It's fairly easy to find where to edit the code: we saw in Jadx that the section we want to change is in a try block, so we can just look at the byte code between `:try_start_0` and `:try_end_0`. Replacing it is a bit harder, it requires some familiarity with smali and assembler programming in general. The documentation for the instructions can be found on the [Android website](https://source.android.com/docs/core/runtime/dalvik-bytecode). Here we can replace the code between the try labels by:

```smali
    iget-object v3, p0, Lcom/example/tuto/MainActivity$1;->this$0:Lcom/example/tuto/MainActivity;
    invoke-virtual {v3}, Lcom/example/tuto/MainActivity;->getCacheDir()Ljava/io/File;
    move-result-object v3
    const-string v2, "Voix ambigue d\'un coeur "
    new-instance v1, Lcom/example/tuto/Checker;
    invoke-direct {v1, v2, v3}, Lcom/example/tuto/Checker;-><init>(Ljava/lang/String;Ljava/io/File;)V

    const-string v2, "FLAG{_________}"
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B
    move-result-object v2
    invoke-virtual {v1, p1, v2}, Lcom/example/tuto/Checker;->check(Ljava/lang/String;[B)Z
    move-result p1

    if-eqz p1, :cond_0
    .line 107
    iget-object p1, p0, Lcom/example/tuto/MainActivity$1;->this$0:Lcom/example/tuto/MainActivity;
    const-string p2, "Congrat"
    const-string v0, "You got it!"
    invoke-virtual {p1, p2, v0}, Lcom/example/tuto/MainActivity;->popup(Ljava/lang/String;Ljava/lang/String;)V
```

You can also remove the `:try_*` and `.catch *` lines to help Jadx a little.

You can now repackage the application:

```sh
apktool b repackaged_tuto -o repackaged_tuto.apk
```

You will notice that the new application cannot be installed. Indeed, applications are signed, and you just modified it. You need to resign the application. First you have to generate a key, using `keytool` (available in any java SDK), align the files in the application then sign it (with `zipalign` and `apksigner` from the build-tools):

```sh
keytool -genkeypair -validity 1000 -dname "CN=SomeKey,O=SomeOne,C=FR" -keystore ToyKey.keystore -storepass Pssw0rd -keypass Pssw0rd -alias SignKey -keyalg RSA -v
zipalign -v -f 4 repackaged_tuto.apk repackaged_tuto.aligned.apk
apksigner sign -ks ./ToyKey.keystore --v2-signing-enabled true --in repackaged_tuto.aligned.apk --out repackaged_tuto.signed.apk --ks-pass pass:Pssw0rd
```

The application is now singed, but still, you cannot installed it becasue the signature key does not match the one of the initial application. You need to uninstall the application on the phone first.

```sh
adb uninstall com.example.tuto
adb install repackaged_tuto.signed.apk
```

The application should work as before, but now if you reverse it with Jadx, you should see in `MainActivity.java`:

```java
String obj = ((EditText) inflate.findViewById(R.id.entry)).getText().toString();
if (obj.length() == 15 && obj.startsWith("FLAG{") && obj.endsWith("}") && new Checker("Voix ambigue d'un coeur ", MainActivity.this.getCacheDir()).check(obj, "FLAG{_________}".getBytes())) {
    MainActivity.this.popup("Congrat", "You got it!");
} else {
    MainActivity.this.popup("Failure", "Invalide Licence Key");
}
```

And now, `repackaged_tuto.signed/sources/com/example/tuto/Checker.java` is available.

## More Dynamic analysis: Frida

When looking at `repackaged_tuto.signed/sources/com/example/tuto/Checker.java`, you will see that the 10 first characters of the string are checked against some value, and then the value is once again checked with a class loaded from another file.

We could insert a log statement in the smali of `Checker.check` to get the string it is compared to, but we saw it takes time to write working smali. 

We have another issue, the next check is loaded from `deciphered.dex` in the cache directory. This file is the decrypted value `ciphered`, and is delated right after loading its content. We could manually extract the information used to decipher the file from the application, but once again, it is a bit long and error prone. Instead, we will use frida to inject code at runtime and intercept information.

Firda is tool that inject itself inside the memory of an application and intercept calls to methods to run hooks in javascript. Frida is a powerfull tool with a lot more features but unfortunately, it has a limited documentation.

First you need to [setup frida](https://frida.re/docs/android/). Get `frida-server-16.6.4-android-x86_64.xz` (or `frida-server-16.6.4-android-arm64.xz` if you are on an ARM computer), or the last version on [github](https://github.com/frida/frida/releases) and put run it on the phone. To keep it running, either use `adb shell /data/local/tmp/frida-server &` or run the command in another terminal. Don't forget to be root:

```sh
unxz frida-server-16.6.4-android-x86_64.xz
adb root
adb push frida-server-16.6.4-android-x86_64 /data/local/tmp/frida-server
adb shell chmod 755 /data/local/tmp/frida-server
adb shell /data/local/tmp/frida-server &
```

Now that the server is running, you need to connect to the frida server. There is a python package that bundle the frida tools for that. The python package **must** be synchronized with the server (remove `==13.6.1` if you use the last version of frida).

```sh
python3 -m venv venv
source venv/bin/activate
pip install frida-tools==13.6.1
```

Let's first start with `frida-trace`. It will generate the hooks automagically and log the method calls. En theory, logging `java.lang.String.equals()` should be enough, but the default handler will not print the string `this` and considers the argument to be generic `Object`, preventing the logger to print it as a string. We will see how to make our own hooks later, but right now, we will use a workaround (you can also try to edit the faulty hook via the web UI). Notice in the code of `Checker.check` that the two values compared are returned by `java.lang.String.substring()`. This means we can hook this method instead:

```console
$ frida-trace -U -N com.example.tuto -j 'java.lang.String!substring'
Instrumenting...
String.substring: Loaded handler at "/tmp/tmp.Jro9Fii3w3/__handlers__/java.lang.String/substring.js"
Started tracing 1 functions. Web UI available at http://localhost:34093/
           /* TID 0x18b7 */
  3338 ms  String.substring(0, 10)
  3338 ms  <= "FLAG{....."
  3338 ms  String.substring(0, 10)
  3338 ms  <= "FLAG{Hello"
```

(Be careful: *you need to start the application before running `frida-trace`*, but you need to run the trace before entering the key. Notice also that `Checker.check()` is not called if the key is not 15 char long and does not starts with `FLAG{` and end with  `}`, `FLAG{.........)` for example)

We now know that the key starts with `FLAG{Hello`. We already know from static analysis the class details of class loading, but let's see if we can find them again from tracing some calls.

They are several [class loaders in android](https://developer.android.com/reference/java/lang/ClassLoader), and it is not so simple to intercept all of them. For now, let's start with `PathClassLoader` and the call to `java.lang.ClassLoader.loadClass()`:

```console
$ frida-trace -U -N com.example.tuto -j 'java.lang.ClassLoader!loadClass' -j 'dalvik.system.PathClassLoader!*'
Instrumenting...
ClassLoader.loadClass: Auto-generated handler at "/tmp/tmp.Jro9Fii3w3/__handlers__/java.lang.ClassLoader/loadClass.js"
PathClassLoader.$init: Loaded handler at "/tmp/tmp.Jro9Fii3w3/__handlers__/dalvik.system.PathClassLoader/_init.js"
Started tracing 2 functions. Web UI available at http://localhost:44347/
           /* TID 0x195f */
 23612 ms  ClassLoader.loadClass("com.android.org.conscrypt.OpenSSLEvpCipherAES$AES$CBC$PKCS5Padding")
 23612 ms  <= "<instance: java.lang.Class>"
 23613 ms  PathClassLoader.$init("/data/user/0/com.example.tuto/cache/deciphered.dex", "<instance: java.lang.ClassLoader, $className: dalvik.system.PathClassLoader>")
 23614 ms  ClassLoader.loadClass("com.example.tuto.Checker2")
 23614 ms     | ClassLoader.loadClass("com.example.tuto.Checker2", false)
 23614 ms     | <= "<instance: java.lang.Class>"
 23614 ms  <= "<instance: java.lang.Class>"
 ```
(Don't forget to use a valid key format, like `FLAG{Hello....}`)

Here we can see that a `PathClassLoader` was initialized from `/data/user/0/com.example.tuto/cache/deciphered.dex` and the class `com.example.tuto.Checker2` was loaded dynamically. Unfortunately `adb shell ls /data/user/0/com.example.tuto/cache/` show that `decipher.dex` does not exist anymore.

We will need to intercept the file after its creation and before it delegation. This will require to write our own Frida hook. First, let's create a python script to interact with Frida:

```python
import frida

# Callback to receive the data send by the `send` function in javascript.
def on_message(message, data):
    if message['type'] == 'send':
        print(f"[*] {message['payload']}")
    elif message['type'] == 'error':
        print(message["description"])
        print(message["stack"])
    else:
        print(message)

# Javascript code run in the app process.
jscode = """
Java.perform(() => {
  // Get the PathClassLoader class
  const PathClassLoader = Java.use('dalvik.system.PathClassLoader');
  // Get the constructor function
  const constructor = PathClassLoader.$init.overload('java.lang.String', 'java.lang.ClassLoader');
  // Override the constructor implementation
  constructor.implementation = function (path, class_loader) {
    // Send data back to the python client
    send('Load code from: ' + path);
    // run the original implementation of the constructor
    return this.$init(path, class_loader);
  };
});
"""

# Connect to the frida server on the emulator
device = frida.get_usb_device()
# Spawn the app process
pid = device.spawn(["com.example.tuto"])
# Attach a session to the app process
session = device.attach(pid)
# Add the script to the app process
script = session.create_script(jscode)
# Define the callback to `send`
script.on('message', on_message)
# Load the script
script.load()
# Start the process
device.resume(pid)
# Frida run in another thread, that will terminate with the main thread.
# this line prevent the main thread from terminating.
input()
```

This script will open the application (instead of attaching itself to a running process like we did with `frida-trace`). For now, the hook will just send to python the location of the .dex file, just before loading it with a class loader.

Now we need to get the file. We have different ways, for instance we can copy the file somewhere else then downloading it with `adb pull`:

```js
const Files = Java.use('java.nio.file.Files');
const Path = Java.use('java.nio.file.Path');
const StandardCopyOption = Java.use('java.nio.file.StandardCopyOption');
const original = Path.of(path, []);
const copy = Path.of("/data/user/0/com.example.tuto/cache/saved.dex", []);
const option = StandardCopyOption.REPLACE_EXISTING.value;
const options = Java.array("java.nio.file.StandardCopyOption", [option]);
Files.copy(original, copy, options);
```

Now you should see the file after entering the key:

```console
$ python run_frida.py
[*] Load code from: /data/user/0/com.example.tuto/cache/deciphered.dex

$ adb ls /data/user/0/com.example.tuto/cache/
000045f9 00001000 6791197a .
000041c0 00001000 6790b9ff ..
00008180 000004c0 6791196c ciphered
00008100 000004bc 6791197a saved.dex
00008180 00000f5c 6791196c checker.dex
$ adb pull /data/user/0/com.example.tuto/cache/saved.dex
/data/user/0/com.example.tuto/cache/saved.dex: 1 file pulled, 0 skipped. 2.3 MB/s (1212 bytes in 0.000s)
$ file saved.dex
saved.dex: Dalvik dex file version 035
```

Alternatively, the file can directly be send to the python client:

```js
const Files = Java.use('java.nio.file.Files');
const Path = Java.use('java.nio.file.Path');
const Base64 = Java.use('android.util.Base64');
const dexfile = Path.of(path, []);
const dex = Files.readAllBytes(dexfile);
const b64 = Base64.encodeToString(dex, Base64.DEFAULT.value);
send("dex: " + b64)
```

```python
import base64

# Callback to receive the data send by the `send` function in javascript.
def on_message(message, data):
    if message['type'] == 'send' and message['payload'].startswith("dex: "):
        with open("deciphered.dex", "wb") as file:
            file.write(base64.b64decode(message['payload'].removeprefix("dex: ")))
        print("[-] dex file saved at 'deciphered.dex'")
    elif message['type'] == 'send':
        print(f"[*] {message['payload']}")
    elif message['type'] == 'error':
        print(message["description"])
        print(message["stack"])
    else:
        print(message)
```

```console
$ python run_frida.py
[*] Load code from: /data/user/0/com.example.tuto/cache/deciphered.dex
[-] dex file saved at 'deciphered.dex'

$ file deciphered.dex
deciphered.dex: Dalvik dex file version 035
```

We can now reverse `deciphered.dex`, or just continue to use Frida and hook `String.equals()`:

```js
const String = Java.use('java.lang.String');
const equals = String.equals.overload('java.lang.Object');
equals.implementation = function (other) {
    const v1 = this.toString();
    const v2 = other.toString();
    if (v1.includes("FLAG")) {
        send('intercepted "' + v1 + '" == "' + v2 + '"');
    }
    return this.equals(other);
}
```

```console
$ python run_frida.py
[*] intercepted "FLAG{Hello" == "FLAG{Hello"
[*] Load code from: /data/user/0/com.example.tuto/cache/deciphered.dex
[-] dex file saved at 'deciphered.dex'
[*] intercepted "FLAG{Hello....}" == "FLAG{HelloVoid}"
```
