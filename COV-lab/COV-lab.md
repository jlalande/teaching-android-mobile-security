# COV lab - Developing covert channels


![JFL](images/overcoming2.jpg) 

## Prerequisite

For this exercise, any smartphone or emulator should work. 

## Introduction

In this exercise of the tutorial, we will build a covert channel between two applications. The idea is to transmit data between one application CCSender to an application CCReceiver without any legal communication channel !

Such covert communication enable two parts of a malware to transmit data without being discovered, as represented in this screenshot:


![JFL](images/covert2.png) 

## Let's go !

### Step 1: Create the projects

Create two Android projects. The first one will implement CC sender and the second one CC receiver.

### Step 2: Implement a sender

In CC sender, we propose to implement a task that will regularly load or unload the disk for transmitting data. First, the activity can create a file:

```java=
f = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "test");
```

Then, in a button, implement a loop that reads each byte of a string and writes `new byte[b * 100000]`.
  
```java=
String s = new String("Mon message");
if (s != null && !"".equals(s)) {
  for (byte b : s.getBytes()) {
    ...
    Thread.sleep(500);
  }
```
You can delete the file if needed by calling `f.delete();`.

### Step 2: Implement a receiver

In CC receiver, implement a TimerTask that will regularly check the available disk space. With the observed variation, you can infer the encoded byte. 

You can get the free space by calling:
```java=
long c = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getFreeSpace();
```
In order to make a repetitive task, you can use a TimerTask:

```java=
TimerTask t = new TimerTask() {
            @Override
            public void run() {
            ...
            };
Timer timer = new Timer();
timer.schedule(t, new Date(), 500);
```

By recording the freespace, you should be able to deduce from any variation, the encoded byte.

### Step 3: Synchronize both sender and receiver

The difficulty is to know when the a byte begins in the flow. You can use a special sequence e.g. 00000000 to setup a "starting point".

### Step 4: Display the received data

Display in your CC receiver app the received bits. Then decode the bits to display the received  String.

:+1: 

## Conclusion

In this exercice, the difficulty step is to synchronize the two applications before starting the transmission. Other implementations can use another channel for synchronizing. Additionally, some channels may fail depending of the used smartphone, the available space (disk or memory). Obtaining a high throughput is also a difficult challenge. If the TimerTask goes quick, the receiver may fail to be synchronized (at some points) introducing errors in the transmission.

