import sys

if len(sys.argv) != 2:
    print "Usage: %s xored_file"
    sys.exit(-1)

file_b = bytearray(open(sys.argv[1], 'rb').read())
xord_byte_array = bytearray(len(file_b))

for i in xrange(len(file_b)):
	xord_byte_array[i] = file_b[i] ^ 0x42

open(sys.argv[1], 'wb').write(xord_byte_array)
