#!/bin/bash

# ./packing_scripts.sh ~/AndroidStudioProjects/PackLAB/app/build/outputs/apk/release/app-release-unsigned.apk

# First arg is the apk location
if [ "$#" -ne 1 ]; then
    echo "Usage $0 apk"
    exit -1
fi

# apktool d the apk
rm -r /tmp/packing
apktool d -s -r -o /tmp/packing/ $1

# Copy the classes.dex into butterfly.png
cp /tmp/packing/classes.dex /tmp/packing/assets/butterfly.png

# Xor butterfly.png
python xor_file.py /tmp/packing/assets/butterfly.png

# Nop the method in classes.dex
python nop_dex.py /tmp/packing/classes.dex

# apktool e the apk
apktool b -f -o /tmp/new.apk /tmp/packing

# Resign the apk
# The key has been generated using:
# keytool -genkey -keystore packing.keystore -alias packing
# keytool -importkeystore -srckeystore packing.keystore -destkeystore packing.keystore -deststoretype pkcs12
echo "The password is aaaaaa"
jarsigner -keystore packing.keystore -verbose /tmp/new.apk packing
