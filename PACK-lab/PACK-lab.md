# PACK lab - Packers

![Cheminee](images/cheminee.png)

## Prerequisite

For this exercise, you can use an Android emulator or a
smartphone. Android version **7.0** or **7.1**  is **mandatory**.

### Required tools

- [Jadx](https://github.com/skylot/jadx/releases)
- Alternative: [BytecodeViewer](https://github.com/konloch/bytecode-viewer/releases)
- [radare2](https://github.com/radare/radare2): reversing assembly files (the most recent version should be ok - version 5.5.5 is working fine).

## Introduction

In this exercise of the tutorial, we will study a packed malware. The
action performed by the malware should be very easy to reverse:
removing all the contacts. However the method realizing the malicious
part is packed: its bytecode is deciphered at execution time. This
prevents the analyst to use static tools as the malicious code is unavailable when analyzing the APK file.

The lab helps you to understand this and to overcome this limitation by finding the location of the malicious code.

## Let's go !

### Step 1 - Observing the malware

- Install the malware [packed_malware.apk](packed_malware.apk) in a smartphone/emulator running Android version
7.0. **WARNING**: do **NOT** install and run this application on your own
smartphone.
- Create few contacts.
- Run the malware PackLAB. At the beginning of its execution, grant the asked
permission.
- Check  the contacts you created before: you should see that they all
have been deleted!!

Now that we have guessed the behaviour of this malware, we are going the
verify our assumptions by reversing it.

### Step 2 - Analyze the DEX code

To watch the code of the malware, use the Jadx tool. The reversing is
pretty easy since there is only one relevant class 'MainActivity'.
When the activity is created, it checks for READ_CONTACTS and
WRITE_CONTACTS permissions. Once they are granted, it invokes the
method `beEvilSetup`. This method calls `decodeMethod` and then `beEvil`. If we look at the `beEvil` code, the decompilation gives nothing. So we can legitimately think that the method's bytecode is set up by the `decodeMethod`
invocation.

This method is declared 'native':

```java
public native void decodeMethod(String paramString1, String paramString2);
```

A native method is a method implemented in assembly instead of
bytecode. Because assembly differs from one architecture from another,
it differs from one phone to another. Then the application has to
embedded the assembly of the native method for all architectures. Consequently, extra files are located in the library directory of the APK for such binary codes. The name of the file used by the malware can be identified looking at the code
`System.loadLibrary("native-lib");`.

We have understood everything that was possible using the JAVA
bytecode. It is now time to reverse the library `native-lib` to understand how
`decodeMethod` retrieves the real `beEvil` bytecode.

### Step 3 - Analyse the embedded library

The library file is stored in the `lib` directory of the APK. Each
sub-directory corresponds to a specific architecture. We are going to
analyse the arm64-v8a version of the library, the most common
architecture on smartphones.

To analyse it we are going to use [radare2](https://github.com/radare/radare2) *version 2.30*.

First launch radare2.
```bash
$ radare2 lib/arm64-v8a/libnative-lib.so
```

Then ask radare2 to analyze the library. The type of analysis will contain:
- the name of the symbols
- the string table
- etc.

For launching the analysis type:
```bash
aaaa
```

Android uses symbols to match native methods to assembly code. We want to see the code of the decode method. Tough, we need to get the address of the decode method. To get this address, we look at the symbol table that makes the correpondance between methods and addresses.

First, list all possible flagspaces that radare2 can analyze:
```bash
fs
```

Select the symbols table and print it:
```bash
fs symbols
f
```

In the list you should get the address of `sym.Java_pg_packlab_MainActivity_decodeMethod`.

Jump to this address.
```bash
s sym.Java_pg_packlab_MainActivity_decodeMethod
```

Now we are going to ask radare2 to display the Control Flow Graph
(CFG) of this method.
```bash
VV
```

To walk around the graph use the keyboard
arrows (down and up). To exit type two times on 'q'. Highlighting text can be done with "/".

For example, we extracted the following part of the CFG:

![cfg](images/radare2.png)

In this extract, three basic blocks are displayed and the first one can jump to the two other ones. At execution time, only one of the two paths can be taken. The white number in a basic block is its address (address of the first instruction). The other lines are the executed code of the basic block. You have to be fluent in ARM64 language :).


At the first basic block (address 0xf94), the code loads an asset using
`bl sym.imp.AAssetManager_open` (bl means Branch with Link). The name of the asset ("butterfly.png") is stored in the register x1 few lines above. This is a suspicious code, as the decode method should not use picture files (and no picture is displayed in the app). 

Try to display this picture by unzipping the apk file to get this asset. It seems to be broken. Use the 'file' command to check the file type.

At this point, you should investigate with radare2 the rest of the method to understand how is used butterfly.png. To shorten this tutorial, we give the insight of the operations the decode method perfoms:

- In the 0x1194 basic block, you should observe that a loop is performed (green arrow).
- In the code, the supsicious instructions is a sequence "load" (ldrb), "xor" (*eor*), "store" (*strb*). More information about *eor* can be found [here](https://developer.arm.com/documentation/ddi0487/gb/).
- With deeper analysis of other basic blocks, we can learn that x13 contains the address of the asset "butterfly.png". We conclude from these two points that the code is xoring the content of butterlfy with a key.
- Looking at the preceeding basick block, you  should be able to determine the used key.

We can now leave radare2, using the 'q' command, and retrieve the
content of the `beEvil` method. We have discovered that, in this simplified case, some binary code is unciphered with a constant integer key using a XOR operation.

### Step 4 - Retrieve the initial code

Unxor "butterfly.png" using the following python2 script:

```python
with open("butterfly.png", 'rb') as fp:
    file_b = fp.read()
xord_byte_array = bytearray(len(file_b))

try:
    for i in range(len(file_b)):
        xord_byte_array[i] = file_b[i] ^ key
except Exception:
    print("You have to understand and patch this code with a real key !")

with open("unpacked_malware.dex", 'wb') as fp:
    fp.write(xord_byte_array)
```

You can check the header of the obtained file and verify that it is a Dalvik dex file.
```bash
$ file unpacked_malware.dex
```

### Step 5 - Reverse the payload 

You can now replay the step 2 on this dex file and see the real
content of the `beEvil` method!

## Conclusion

This tutorial has presented the reverse of a malware that use packing techniques for protecting its code. The bytecode of the payload is populated at runtime, which defeat any automatic analysis working on the file *classes.dex*. The security analyst should have solid skills in ARM64 reversing to analyze the used packing method that obfuscates the payload.


