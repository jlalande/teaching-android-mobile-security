# Exercise 2: Hacking a spyware

![Archer](images/surveillance.jpg)

## Prerequisite

### Smartphones

For this exercise, you should have one of the following smartphones:

- Emulator 4.1
- A newer version may work.

### Tools

- [Jadx](https://github.com/skylot/jadx/releases)
- [Apktool](https://github.com/iBotPeaches/Apktool/releases)

## Malware

**KitchenService**: 

- SHA 256: 2fbc32387f9b5c5a8678af3a76c0630ba4d04fd520b21782642a517794063f05
- get if from [Koodous](https://koodous.com/apks/2fbc32387f9b5c5a8678af3a76c0630ba4d04fd520b21782642a517794063f05)

## Introduction

In this exercise of the tutorial, we will study the **KitchenService malware** which is a **spyware**. It leaks some user information to the internet and display web pages. What a really cool malware to study !!!

The goal of this exercise is to understand how works the malware by reverse engineering its code. Then we will try to capture the data when the malware leaks them.

## Let's go !

### Step 1 - Reverse and study the malware

With Jadx, open the malware bytecode.
 
Study the required permission in the Manifest file. You should get a better idea of the malware type by reading the asked permissions. You also see the declaration of a service *KitchenTimerService*.

Study the class KitchenTimerService. You see that the service launches futur tasks (TimerTask) that fires Intents with action "Kitchen Timer Service". At this step, we can wonder what compnents will capture this Intent.

Open the *Main*class. The first place to look is the *onCreate()*method, which is called when the application is launched by the user. You'll see several things:
- the app get classes for making vibrate the phone and to locate it. Yes, the phone is really vibrating ! (not sure for an emulator :p)
- the app starts the service *KitchenTimerService*
- the app register a receiver for the message "Kitchen Timer Service": thus, for this message the *KitchenTimerReceiver* will be called.

Then, you have to inspect the KitchenTimerReceiver to see what happens when the intent is received. What information the malware is stealing? How these information are sent to the attacker ?
  
Finally, the malware takes care to reprogram a task with a call to *Main.this.kitchenTimerService.schedule(120000L);*.

### Step 2 - Execute it

Launch an Android Emulator or start your smartphone.

Install the malware:

  ```bash
    adb install Kitchen_Service.apk
  ```
  
In case the install fails because the app is already installed with another signature, try uninstalling the old version:

  ```bash
    adb uninstall com.example.android.service
  ```
  
The malware appears with an anonymous icon. You should observe the malware leaking the data.

Now, we want to spy the HTTP requests that are performed by the malware.

### Step 3 - Setup a local HTTP server

Setup a local web server that will print the HTTP requests (for example based on https://gist.github.com/1kastner/e083f9e813c0464e6a2ec8910553e632)

Check that you get the full url in the console when your browser goes to:

    http://0.0.0.0:8080/myfile.php?a=65

### Step 4 - Change hostnames in the malware

The malware use the domain 14243444.com to disclose some information. We need to force the malware to use our web server. If you have control over your DNS server, you can easily tune this. This trick will not work with modern applications, because the use of HTTPS is now mandatory on Android.

When you cannot modify the DNS used nor patch the TLS CA used by Android, you can patch the application

We will use apktool to do that. See the [old subject](MAL-labs/02-spyware.old.md) for an solution using Soot.

Unpacked the malware:

```
apktool d Kitchen_Service.apk -o Kitchen_Service_unpacked
```

In `Kitchen_Service_unpacked/smali/com/example/android/service/Main\$KitchenTimerReceiver.smali`, replace the all instance of `14243444.com` with `10.0.2.2:8080` (10.0.2.2 is the IP of the host computer from the point of view of the emulator):


```bash
sed 's#http://14243444.com#http://10.0.2.2:8080#g' -i Kitchen_Service_unpacked/smali/com/example/android/service/Main\$KitchenTimerReceiver.smali
```

Repackage the malware:

  ```bash
  apktool b Kitchen_Service_unpacked -o Kitchen_Service_patched.unaligned.apk
  zipalign -v -f 4 Kitchen_Service_patched.unaligned.apk Kitchen_Service_patched.unsigned.apk
  keytool -genkey -v -dname "CN=SomeKey,O=SomeOne,C=FR" -keystore lab.keystore -storepass android -alias SignKey -keypass android -keyalg RSA -keysize 2048 -validity 10000
  jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore lab.keystore -storepass android -keypass android -signedjar Kitchen_Service_patched.apk Kitchen_Service_patched.unsigned.apk SignKey

  ```


### Step 5 - Launch the malware and see requests !

Uninstall the apk and reinstall it:

```
adb uninstall com.example.android.service
adb install Kitchen_Service_patched.apk
```

Launch the malware. You should capture the leaks sent by the malware !

## Conclusion

The capture of the HTTP traffic enables to see the parameters of the requests. It could also be used to answer these requests, for example if the malware is waiting for commands. For dangerous or unknown malware, **you should not connect the malware to internet** ! For more complexe malware, it would be more safe to implemet a controlled network with DNS and Firewall to filter requests and stay confined in a local network.

