
# Exercise 3 : Monitoring adware

![Woman looking a malware](images/woman-camera.jpg)

## Malware:

**Mobidash**:

- SHA 256: b41d8296242c6395eee9e5aa7b2c626a208a7acce979bc37f6cb7ec5e777665a
- get if from [Koodous](https://koodous.com/apks/b41d8296242c6395eee9e5aa7b2c626a208a7acce979bc37f6cb7ec5e777665a)

## Prerequisite

For this exercise, we need a **custom rom** that includes AndroBlare. AndroBlare traces all the information flows between processes, files and sockets. The kernel module (Blare), ported to android (AndroBlare), monitors these flows. This is why you need a pre-installed Nexus that already contains AndroBlare.

## Introduction

In this exercise we will analyse the activities of a malware called **Mobidash** (SHA 256: ). The goal is to **observe its activities** at operating system level. The difficulty is that this malware will not trigger at first run.


**Mobidash is an adware** discovered in January 2015 that covers its malicious activities with a benign app: a card game. When the malware triggers itself, it displays unwanted ads each time the user unlocks the screen. To evade dynamic analysis tools during an analysis that would occur after the installation, the malware waits several days or even weeks before executing its malicious code. The user would also not suspect a fresh installed application several days later.

## Let's go !

### Step 1: connect your device

You should see your device by doing:

  ```bash
  adb devices
  List of devices attached
  XXXXX	device
  ```

### Step 2: clean your Nexus

Before executing a malware, we need to clean the smartphone of any artifact that remains from a previous experiment.

**1st case**: command oriented restoration.

   * On a fully booted phone access reboot to TWRP using adb:

`adb reboot recovery`

**2nd case**: manual oriented restauration

  * Power off the phone
  * Hold *Vol-* **and** *Power Button* to access the bootloader
  * Into the bootloader use *Vol+/Vol-* to see **Recovery Mode**
  * Validate this choice with *Power Button*

**Deploy the backup for flashing the phone**

  * Once you are on TWRP tap restore and select *hammerhead.backup*
  * Swipe to restore and wait until the process succefully ends.
  * Tap reboot system. The blarized rom is succefully installed.

### Step 3: connect the phone to internet

Connect your smartphone to the internet using a Wifi network or the provided access point. This is mandatory for such a malware that will download ads from remote servers.

### Step 4: install MobiDash and configure Androblare

  The first thing to do is to install the malware. In our case this is ModiDash.

```bash
  adb install com.cardgame.durak.apk
```

**DO NOT LAUNCH THE MALWARE YET !**
  
  If the malware is successfully installed you should have a new application called **Durak Pro** but we need to prepare Blare to audit the malware.

  You need to apply some Androblare configuration after you installed it. An automated script *finalize.sh* apply thoses settings.

  ```bash
  adb install BlareLogger.apk
  adb shell mount -o remount,rw /system
  ./finalize.sh
  ```

  Now you need to tell AndroBlare which file you want monitor:

  ```bash
  adb shell setinfo /data/app/com.cardgame.durak-1.apk 1
  adb reboot
  ```


  After the reboot, on the smartphone, launch the BlareLogger application and hit the "Start Log" button. Logs are created on */data/data/org.blareids.logger/logs/log_name.blarelog*. Check that logs appear in the Blare folder:
  
  ```bash
  adb shell ls /data/data/org.blareids.logger/logs
  ```


### Step 5: launch the malware

Launch the **Durak Pro** application. You should see a card game. You can play with it, it's fully functional !

Blare is auditing the malware and propagating the observed flows. It produces log. You can see the log on the phone:

  ```bash
  adb shell cat /data/data/org.blareids.logger/logs/*.blarelog
  ```

You should see some policy violation rules:

```bash
<6>[   74.757664] [BLARE_POLICY_VIOLATION] process .cardgame.durak:.cardgame.durak 787 > file /data/data/com.cardgame.durak/shared_prefs/com.cardgame.durak_preferences.xml 57598 > itag[1]
```

Such a rule says that the process cardgame.durak with PID 787 has produced a write flow in the file /data/.../com.cardgame.durak_preferences.xml with the tag number 1.

### Step 6: installing the requirements

For exploiting the produced logs, you need the folowing requirements:


```
pip3 install argparse
pip3 install networkx
pip3 install pydot
```

### Step 7: extraction, conversion, visualization

Extract the log:

```bash
adb shell ls data/data/org.blareids.logger/logs
adb pull /data/data/org.blareids.logger/logs/log.........
```

Now we need to convert the *blarelog* file to a graph in order to visualize it.  For this tutorial we will use *.dot* files as a lot of tools exist to visualize such a format. 

```bash
python3 grodd3logparser.py -i <log_name> -t dot
```

Open the resulting dot with [zgrviewer](http://zvtm.sourceforge.net/zgrviewer.html) or any other tool that displays dot files:

```bash
cd zgrviewer
./run.sh
```

and open the generated dot file.

You should see the graph of all processes, files and sockets that the application has contaminated. This graph is unusable, because there are a lot of nodes in this graph. We propose to filter the nodes that are not useful for this tutorial (mainly the ones that contaminated the android system i.e. the system_server process). A quick and dirty solution is given below:

```bash
# filter nodes and edges from the log based on their name
cat <log_name>.blarelog | grep -v "[C\|c]ache" | grep -v "[S\|s]ystem_server" > filteredlog.blarelog
```

Now, convert again the log file and visualize the resulting graph.

```bash
python3 grodd3logparser.py -i filteredlog.blarelog -t dot
```

Focus on the **red** ellipse. This is the process that has been launched from our tagged apk (*cardgame.durak*). Explore the graph and try to understand what it has done:

- Ellipse are processes
- Squares are files or sockets

You should obtain:

[![Silent graph](images/cardgame_graph-silent.png)

You should not observe any socket connection: the malware **has not been triggered** and is waiting silently. We will know trigger it.



### Step 8: trigger the malicious behavior

You may have observed one important things: the access/creation of the file `/data/data/com.cardgame.durak/shared_prefs/com.cardgame.durak_preferences.xml com.cardgame.durak_preferences.xml`. This **XML** is the malware configuration file. Maybe it has something cool in it ?

We are going to see what's inside. Pull out this file from the phone:

```bash
adb pull /data/data/com.cardgame.durak/shared_prefs/com.cardgame.durak_preferences.xml
gedit com.cardgame.durak_preferences.xml
```

The most important line is at the end **long name="mobi.dash.extras.overapp.AdsOverappWaiter.waitStartTime" value="33316051155" />** with a big number as value. In fact MobiDash delays its execution: it decrements this number until it reachs **0** and then trigger its execution.
We are going to force its triggering by replacing the number by a *0* (use `gedit, nano, vi...`)

Change:
```
long name="mobi.dash.extras.overapp.AdsOverappWaiter.waitStartTime" value="33316051155" />
```
Into:
```
long name="mobi.dash.extras.overapp.AdsOverappWaiter.waitStartTime" value="0" />
```

After that we need to push this modified file to the phone and reboot it immediately in order to prevent the modification of this file by the malware.

```bash
adb shell "/system/bin/busybox rm -rf /data/data/org.blareids.logger/logs/*"
adb push com.cardgame.durak_preferences.xml /data/data/com.cardgame.durak/shared_prefs/com.cardgame.durak_preferences.xml && adb reboot
```

Once the phone has rebooted unlock it and wait BlareLogger screen. Return Home, lock the phone and unlock it. If you are lucky, you should observe **lots of undesired Ads**.

Exract logs again and convert it to a *.dot* file. Once finished open the *.dot* with **zgrviewer**. You can see the **creation of sockets, access to android.browser and many more** !

## Conclusion

You have done your first dynamic analysis of an adware. You can see the same result with a better look and feel of the [graph](http://kharon.gforge.inria.fr/dataset/MobiDash_sample_com.cardgame.durak.html) on the  Kharon project. Check:

[http://kharon.gforge.inria.fr/dataset/malware_MobiDash.html](http://kharon.gforge.inria.fr/dataset/malware_MobiDash.html)



