import re
import networkx as nx
import argparse
import os

# To parse the blare log
# we define a REGEX of the DEATH
REGEXBLARELOG=r"<[0-9]>\[[ 0-9\.]*\] \[BLARE_POLICY_VIOLATION] ([\w]*) ([\w\d:\._/-]*) (\d*) > ([\w]*) ([\w\d:\._/-]*) (\d*)( \d*)? > itag\[(\d)*\].*"


# Clean the name of processes
# system_server:Binder_7 => system_server
def cleanname(s):
    parts = s.split(":")
    return parts[0]

def getGraphFromBlareLog(filename, agreggateEdges=False):
    g = nx.MultiDiGraph()

    f = open(filename, "r")
    for line in f:
      if re.match(REGEXBLARELOG,line):
          s = re.split(REGEXBLARELOG, line)
          type1 = s[1] # process, file, socket
          name1 = cleanname(s[2]) # system_server:Binder_7
          pid1 = s[3] # 435
          type2 = s[4] # process, file, socket
          name2 = cleanname(s[5]) # myapk.apk
          pid2 = s[6] # 434
          truc = s[7] # 49444 WTF ?
          tag = s[9] # Blare tag: 99
    
          n1 = (type1, name1, 1)
          n2 = (type2, name2, 1)
          
          if not g.has_node(n1): # Is this node already there ?
              g.add_node(n1)
          if not g.has_node(n2): # Is this node already there ?
              g.add_node(n2)
          if not agreggateEdges or not g.has_edge(n1, n2): # Test if all edges should be added or only not existing
              g.add_edge(n1, n2, flow=[tag], timestamp=[2,1]) # Adding edge
          

    return g



# MAIN 
# ===================================================================================
if __name__ == "__main__":

     # Parser for program args
    argparser = argparse.ArgumentParser(description="Process blare log to build graphs",
                                        usage="grodd3logparser.py [-a] -i input -t <TYPE>")
    argparser.add_argument('-i', help='Specify the input file to be parsed', action='store', required=True,
                           dest='input')
    argparser.add_argument('-t', help='Specify the file output type(s). Supported types are dot, gexf and pickle', required=True, dest='type')
    argparser.add_argument('--na', help='Not aggregating edges', required=False, action='store_false', dest='aggregate')
    argparser.set_defaults(aggregate=True)
    args = argparser.parse_args()
    print("Trying to read " + args.input)
    if not os.path.exists(args.input):
        print ("Provide a valid path")
        quit()

    o_filename = args.input # Same filename for output but with additional extension
    file_format = args.type
    aggregate = args.aggregate

    # Converting Blare log to a graph
    flow_graph = getGraphFromBlareLog(args.input, aggregate)

    # Printing stats
    #for e in g.edges():
    #    print("Edge: " + str(e))
    print("Nodes: " + str(len(flow_graph.nodes())))
    print("Edges: " + str(len(flow_graph.edges())))


    # Save the resulting graph
    if (file_format == "gml"):
        nx.write_gml(flow_graph, o_filename + ".gml")
        print("Graph stored in : " + o_filename + ".gml")
    elif (file_format == "dot"):
        #dot_graph = graphtodot.build_dot_graph(flow_graph, args.cluster_system) 
        #graphtodot.save_dot_to_file(dot_graph, o_filename + ".dot")
        #nx.write_dot(flow_graph, str(o_filename + ".gexf"))
        import pydot
        from networkx.drawing.nx_pydot import write_dot
        write_dot(flow_graph,  str(o_filename + ".dot"))
        print("Graph stored in : " + o_filename + ".dot")
    elif (file_format == "gexf"):
        print("Format you choose is gexf:")
        nx.write_gexf(flow_graph, str(o_filename + ".gexf"))
        print("Graph stored in : " + o_filename + ".gexf")
    elif (file_format == "gpickle"):
        nx.write_gpickle(flow_graph, o_filename + ".gpickle")
        print("Graph stored in : " + o_filename + ".gpickle")
    else :
        print ("Error : unknown format " + str(file_format))

