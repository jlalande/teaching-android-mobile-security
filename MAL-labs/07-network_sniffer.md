
# Exercise 7: Capturing the data leaked by a Spyware

![Snif](images/snif.png)

## Prerequisite

For this exercise, any Android smartphone should work.

You will need a laptop under Linux to build a Wifi access point.

## Malware

**SaveMe**:

- SHA 256: 919a015245f045a8da7652cefac26e71808b22635c6f3217fd1f0debd61d4330
- get it from [Koodous](https://koodous.com/apks/919a015245f045a8da7652cefac26e71808b22635c6f3217fd1f0debd61d4330)

## Introduction

The goal of this exercise is to study the malware **SaveMe**. You can find an overview of this malware on its [kharon page](http://kharon.gforge.inria.fr/dataset/malware_SaveMe.html). This spyware steals the personal data of the users and upload them on a website.

In the following we propose to capture the stolen data without modifying the malware. Instead, we will redirect the network traffic to a server that will reveal the stolen data.

## Let's go ! 

### Step 1: reverse the malware to learn where data go

As previously done in exercise 2, open the malware with BytecodeViewer. Have a look at the following classes in order to find the server name where the malware leaks the data:

- com.savemebeta.Scan.sendsmsdata2
- com.savemebeta.HECKUPD.sendmyinfos
- ...

You'll deduce that the data are sent to *topemarketing.com* with different php files.

### Step 2: capture the network traffic

In order to analyze the leak of data, we should redirect the traffic to a controled web server. We could try to route the network via the USB cable but it requires to have a rooted smartphone.

Thus, in the following, we propose to use your laptop as a **Wifi Access Point** (not the virtualmachine but your real laptop). [All the required steps](https://www.cberner.com/2013/02/03/using-hostapd-on-ubuntu-to-create-a-wifi-access-point/) are given below.

On your real Linux computer do:

Install dnsmask (for distributing an IP to your smartphone) and hostapd (for creating an access point).

  ```bash
  sudo apt-get install dnsmasq hostapd
  ```
  
  Configure dnsmasq in */etc/dnsmasq.d/dnsmasq.conf* with the following:
  
  ```bash
  interface=wlan0
  dhcp-range=10.0.0.2,10.0.0.10,255.255.255.0,12h
  address=/topemarketing.com/10.0.0.1
  ```

You see in this configuration file that the domain used by the malware will be routed to *10.0.0.1*.
Additionally, reroute the domain name *topemarketing.com* by putting in your */etc/hosts* file:

  ```bash
  10.0.0.1       topemarketing.com
  ```

Start dnsmasq:

  ```bash
  sudo /etc/init.d/dnsmasq start
  ```

Now create the file *hostapd.conf* and customize your SSID:

  ```bash
  # interface wlan du Wi-Fi
  interface=wlan0
  # nl80211 avec tous les drivers Linux mac80211 
  driver=nl80211
  # Nom du spot Wi-Fi
  ssid=YOURNAMEDAP
  # mode Wi-Fi (a = IEEE 802.11a, b = IEEE 802.11b, g = IEEE 802.11g)
  hw_mode=g
  # canal de fréquence Wi-Fi (1-14)
  channel=6
  # Wi-Fi ouvert, pas d'authentification !
  auth_algs=1
  # Beacon interval in kus (1.024 ms)
  beacon_int=100
  # DTIM (delivery trafic information message) 
  dtim_period=2
  # Maximum number of stations allowed in station table
  max_num_sta=255
  # RTS/CTS threshold; 2347 = disabled (default)
  rts_threshold=2347
  # Fragmentation threshold; 2346 = disabled (default)
  fragm_threshold=2346
  ```

Modify the */etc/network/interfaces* to setup a static IP:

  ```bash
  auto wlan0
  iface wlan0 inet static
  address 10.0.0.1
  netmask 255.255.255.0
  ```

Disable *network-manager* and remount the network interface:

  ```bash
  sudo service network-manager stop
  sudo ifdown wlan0
  sudo ifup wlan0
  ```

Finally, launch *hostapd* to create the AP:

  ```bash
  sudo hostapd hostapd.conf 
Configuration file: hostapd.conf
Using interface wlan0 with hwaddr 00:22:43:26:3b:c0 and ssid 'YOURNAMEDAP'
  ```
Check your smartphone. You should now be able to connect it to the AP. When the smartphone connects, hostapd says:

  ```bash
  wlan0: STA 24:da:9b:05:b6:xx IEEE 802.11: authenticated
wlan0: STA 24:da:9b:05:b6:xx IEEE 802.11: associated (aid 1)
AP-STA-CONNECTED 24:da:9b:05:b6:xx
wlan0: STA 24:da:9b:05:b6:xx RADIUS: starting accounting session 57756E35-00000000
```

You are now ready to listen to the malware !

### Step 3: cheat the malware

The last step consists in building a fake web server to capture the data leaked by the malware. This can be easily done using the following sript:

  ```python
  from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
from cgi import parse_header, parse_multipart
from sys import version as python_version
if python_version.startswith('3'):
    from urllib.parse import parse_qs
    from http.server import BaseHTTPRequestHandler
else:
    from urlparse import parse_qs
    from BaseHTTPServer import BaseHTTPRequestHandler
    
class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        message =  threading.currentThread().getName()
        self.wfile.write(message)
        self.wfile.write('\n')
        return

    def do_POST(self):
        self.send_response(200)
        self.end_headers()
        ctype, pdict = parse_header(self.headers['content-type'])
        postvars = {}
        if ctype == 'multipart/form-data':
            postvars = parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers['content-length'])
            postvars = parse_qs(
                    self.rfile.read(length), 
                    keep_blank_values=1)
        print(postvars)
        return postvars

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':
    server = ThreadedHTTPServer(('10.0.0.1', 80), Handler)
    print 'Starting server, use <Ctrl-C> to stop'
    server.serve_forever()
```

Launch this multi-threaded web server:

```bash
python exo3-webserver.py
```

On the smartphone, open a browser. enter the url *http://topemarketing.com/blabla*. You should see logs about a GET request appearing in the console.

Finally, launch the malware and play with it. You should capture the leaked data.

:+1: 

## Conclusion

A lot of spyware use basic communication request to leak the data to a server. More sophisticated one use HTTPS, Tor proxys or Cloud storage. With such ones, using network sniffing is no more reliable and solutions getting the information from the malware itself is easier.
