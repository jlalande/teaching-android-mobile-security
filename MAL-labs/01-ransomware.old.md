# Exercise 1: Programming an antidote for a ransomware

![Shake](images/shake.png)

## Prerequisite

### Smartphones

For this exercise, you should have one of the following smartphones:

- An emulator with Android 4.1 Jelly Bean with a Nexus S emulated hardware
- Real Nexus S
- Samsung Galaxy SIII


### Tools

- [BytecodeViewer](https://github.com/konloch/bytecode-viewer/releases)
- Alternative: [Jadx](https://github.com/skylot/jadx/releases)
- [dex2jar](https://github.com/pxb1988/dex2jar/releases)
- [JByteMod](https://github.com/GraxCode/JByteMod-Beta/releases)
- OpenJDK version 11
    
## Malware

**SimpLocker**:

- SHA 256: 8a918c3aa53ccd89aaa102a235def5dcffa047e75097c1ded2dd2363bae7cf97
- get it from [Koodous](https://koodous.com/apks/8a918c3aa53ccd89aaa102a235def5dcffa047e75097c1ded2dd2363bae7cf97)

## Introduction

In this exercise of the tutorial, we will study the **SimpLocker malware** which is a **ransomware**. It **encrypts the user's files** and displays a fullscreen warning asking to pay a ransom to decrypt the file. The malware is connected to the attacker via the Tor network but we do not need to let the malware connect to Tor.

The goal of this exercise is to understand how works the malware by reverse engineering its code. We will let the malware operate and encrypt your files... But then, we will write **an antidote** by modifying the malware to **force it to decrypt the encrypted files** !

## Let's go !

### Step 1: connect your device

You should see your device by doing:

  ```bash
  adb devices
  List of devices attached
  XXXXX	device
  ```
### Step 2: disconnect the phone from the internet

For this exercise we do not need an internet connection. Disconnect your smartphone from the internet.

### Step 3: take a photo of yourself

We need to provide some personal data to the malware. Take a photo of yourself or something if you do not trust totally this tutorial...

Check in the gallery app that the photo is there.

### Step 4: execute the SimpLocker malware

Install the malware (if the device asks you to share data with google, decline):

  ```bash
  adb install simplelocker.apk
  ```
  
In case you need to uninstall the malware, do:

```
adb uninstall org.simplelocker
```

**Launch the malware** by hitting the icon "Sex xonix" or "Locker" (depends of the phone language). You should see a fullscreen message.

Wait 1 minute (the malware encrypts your file). You can check the files to see if the malware produces .enc files:

  ```bash
  # Launch a shell in the smartphone/emulator
  adb shell 
  # Go the folder where images are stored:
  ls /storage/sdcard/DCIM/Camera
  IMG_20160629_115145933.jpg.enc
  IMG_20160629_115148191.jpg.enc
  ...
  ```

Reboot the phone.

  ```bash
  adb reboot
  ```

Uninstall the malware:

  ```bash
  adb uninstall org.simplelocker
  ```
Check the images of the gallery: you should not see your photo anymore. **All your data is gone (encrypted) !!! Damn !!!**

### Step 5: reverse a simple .apk

We propose to use BytecodeViewer which enables to open and decompile an Android application. It proposes several decompilers and basic search capabilities.

Test BytecodeViewer on a demo file [demo1.apk](tools/) that is a small application of three activities (three screens):

  ```bash
  bytecode-viewer demo1.apk
  # or:
  java -jar BytecodeViewer.jar demo1.apk
  ```

Open the *jf.andro.malcon15demo* package and the *MainActivity* class. You should see a decompilation of the bytecode where two buttons are created in the activity. 

This shows how it is **simple** to browse the source code of an app.

### Step 6: reverse the SimpleLocker malware .apk

Reverse the malware bytecode::

  ```bash
bytecode-viewer simplelocker.apk
# or:
java -jar BytecodeViewer simplelocker.apk
  ```

**Boom !** You obtain an exception "**MALFORMED**". This is because one of the files (probably the Manifest) is malformed. This is an obvious protection of the malware developer to prevent any static analysis from classical tools (here apktool which is one of the components of BytecodeViewer). Thus, we will need to do the reverse with manual operations.

Unpack the malware:

  ```bash
  mkdir malware-source
  cd malware-source
  unzip ../simplelocker.apk
  cd ..
  ```

The code is located in **malware-source/classes.dex**. This is a jar file containing all the classes using the dex format. Open the *classes.dex* file that is well formed with BytecodeViewer:

  ```bash
  bytecode-viewer malware-source/classes.dex
  # or:
  java -jar BytecodeViewer malware-source/classes.dex
  ```

Now you can browse the code of SimpleLocker.

The malware developer is a well organized person. All the malicious code is located in the org.simplocker package. Have a look to:

- **FilesEncryptor**: contains the methods that iterate over files and *encrypt()* or *decrypt()* them. 
- **AesCrypt**: contains the encryption implementation. The constructor parameter is the encryption key.

You can use the search box (bottom left) to discover where the *encrypt()* function is called. Search all classes with **Regex** "encrypt()". You should find that the *MainService$5* class launches the encryption. Of course, the ciphering process is performed in background. We can suppose that the deciphering is also performed in a service.

Now, search the calls to *decrypt()*. You will discover that the call is handled in **HttpSender**. Of course, the malware waits for a message from the attacker's server to decipher the files. 

Modifying this part is not an easy task (but would be possible). A better idea is to modify the primary behavior of the malware when *MainService$5* encrypts files. **Instead of calling the *encrypt()* method, we will force *MainService$5* to call the *decrypt()* method**. This way, our new malware will act as an antidote !

### Step 7: convert, edit the classes and repackage

The classes should be converted to the standard .class format in order to be edited, using *dex2jar*. Convert the dex file to a jar file and extracted the classes:

  ```bash
  d2j-dex2jar.sh malware-source/classes.dex
   ```

You should obtain the file *classes-dex2jar.jar*. To change the class *MainService$5.class*, use the bytecode editor JByteMod: 

  ```bash
  java -jar JByteMod-1.8.2.jar
  ```
  
Change the *encrypt()* to a call to *decrypt()*. Save the jar file. 

  
Convert it back to *classes.dex* using *jar2dex*:
 
  ```bash
  d2j-jar2dex.sh classes-patch.jar
  ```
  
Backup and replace the *classes.dex* file:

  ```bash
  # Backup of the original classes.dex
  mv malware-source/classes.dex ./classes-backup.dex
  # Replacing the classes.dex by the new one
  mv classes-patch-jar2dex.dex malware-source/classes.dex
  ```
  
Rebuild the apk and sign it using the procedure below. In case the debug.keystore file is missing, you should force the [generation of this file in Android Studio](https://stackoverflow.com/a/43858904/1156363).

Be careful ! You will **need the version 11 of open JDK** to get the jarsigner version that accepts to make a signature MD5withRSA or SHA1. In later versions of Open JDK, these algorithms have been deprecated and jarsigner is doing RSA signatures. The emulator will fail with RSA signatures.

Check the version of jarsigner:

```bash
jf@mascotte:~/Téléchargements$ which jarsigner
/usr/bin/jarsigner
jf@mascotte:~/Téléchargements$ ls -l /usr/bin/jarsigner
lrwxrwxrwx 1 root root 27 janv. 11 15:31 /usr/bin/jarsigner -> /etc/alternatives/jarsigner
jf@mascotte:~/Téléchargements$ ls -l /etc/alternatives/jarsigner
lrwxrwxrwx 1 root root 48 janv. 11 15:31 /etc/alternatives/jarsigner -> /usr/lib/jvm/java-11-openjdk-amd64/bin/jarsigner
```

When you are sure to have jarsigner from the version 11 of the JDK, you can rebuild your apk and sign it:

  ```bash
  # Rebuilding the apk
  cd malware-source
  rm -rf META-INF
  jar cvf ../simplocker-antidote.apk *
  # Generate a debug keystore:
  keytool -genkey -v -keystore ~/.android/debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000
  # Sign it with the debug key that may be in ~/.android or C:\Users\xxx\.android
  cd ..
  jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore  ~/.android/debug.keystore simplocker-antidote.apk androiddebugkey -storepass android -keypass android
  ```
  
We have now the **antidote** against SimpLocker :smile:

### Step 8: deploy the antidote

Install the **antidote** (if the device asks you to share data with google, decline):

  ```bash
  adb install simplocker-antidote.apk
  ```

**Launch the malware** by hitting the icon "Sex xonix" or "Locker". You should see a fullscreen message.

Wait 1 minute (the antidote deciphers your file). You can check the files to see if the malware deciphers .enc files:

  ```bash
  adb shell ls /storage/sdcard0/DCIM/Camera
  IMG_20160629_115145933.jpg.enc
  IMG_20160629_115145933.jpg
  IMG_20160629_115148191.jpg.enc
  IMG_20160629_115148191.jpg
  ```

Eventually reboot the phone. Uninstall `adb uninstall org.simplelocker`, open the gallery app. Your photos should **reappear** magically ! 

## Conclusion

This malware is a fully functional ransomware that is able to decipher the encrypted files. The command is sent via the Tor network by the attacker, when the user has paid for recovering its files. The main drawback of the malware is the poor quality of the ciphering algorithm that uses a constant as encryption key. A more advanced version of such a malware should generate the key and store it on server side to become a more dangerous malware.

For more information, you can enjoy the Kharon webpage about this malware. The [system flow graph representation](https://cidre.gitlabpages.inria.fr/malware/malware-website/dataset/SimpLocker_sample_fd694cf5ca1dd4967ad6e8c67241114c.html) gives you a view of the Tor process used to anonymize the communication with the attacker. The play button at the end of the pages, replays the interaction that occured between the processes.

