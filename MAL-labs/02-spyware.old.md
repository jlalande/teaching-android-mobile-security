# Exercise 2: Hacking a spyware

![Archer](images/surveillance.jpg)

## Prerequisite

### Smartphones

For this exercise, you should have one of the following smartphones:

- Emulator 4.1
- A newer version may work.

### Tools

- [Jadx](https://github.com/skylot/jadx/releases)
- [ngrok](https://ngrok.com/download)
- [Custom tools](https://gitlab.inria.fr/jlalande/teaching-android-mobile-security/-/tree/master/MAL-labs/tools)

## Malware

**KitchenService**: 

- SHA 256: 2fbc32387f9b5c5a8678af3a76c0630ba4d04fd520b21782642a517794063f05
- get if from [Koodous](https://koodous.com/apks/2fbc32387f9b5c5a8678af3a76c0630ba4d04fd520b21782642a517794063f05)

## Introduction

In this exercise of the tutorial, we will study the **KitchenService malware** which is a **spyware**. It leaks some user information to the internet and display web pages. What a really cool malware to study !!!

The goal of this exercise is to understand how works the malware by reverse engineering its code. Then we will try to capture the data when the malware leaks them.

## Let's go !

### Step 1 - Reverse and study the malware

With Jadx, open the malware bytecode.
 
Study the required permission in the Manifest file. You should get a better idea of the malware type by reading the asked permissions. You also see the declaration of a service *KitchenTimerService*.

Study the class KitchenTimerService. You see that the service launches futur tasks (TimerTask) that fires Intents with action "Kitchen Timer Service". At this step, we can wonder what compnents will capture this Intent.

Open the *Main*class. The first place to look is the *onCreate()*method, which is called when the application is launched by the user. You'll see several things:
- the app get classes for making vibrate the phone and to locate it. Yes, the phone is really vibrating ! (not sure for an emulator :p)
- the app starts the service *KitchenTimerService*
- the app register a receiver for the message "Kitchen Timer Service": thus, for this message the *KitchenTimerReceiver* will be called.

Then, you have to inspect the KitchenTimerReceiver to see what happens when the intent is received. What information the malware is stealing? How these information are sent to the attacker ?
  
Finally, the malware takes care to reprogram a task with a call to *Main.this.kitchenTimerService.schedule(120000L);*.

### Step 2 - Execute it

Launch an Android Emulator or start your smartphone.

Install the malware:

  ```bash
    adb install Kitchen_Service.apk
  ```
  
In case the install fails because the app is already installed with another signature, try uninstalling the old version:

  ```bash
    adb uninstall com.example.android.service
  ```
  
The malware appears with an anonymous icon. You should observe the malware leaking the data.

Now, we want to spy the HTTP requests that are performed by the malware.

### Step 3 - Setup a local HTTP server

Setup a local web server that will print the HTTP requests (for example based on https://gist.github.com/1kastner/e083f9e813c0464e6a2ec8910553e632)

Check that you get the full url in the console when your browser goes to:

    http://localhost:8080/myfile.php?a=65

Launch the ngrok tool (download the binary from [here](https://ngrok.com/download)), to create a tunnel from a regular internet url to your local web server:

    ./ngrok http 8080

Ngrok will create a dynamic url on the ngrok.io domain:

![JFL](http://kharon.gforge.inria.fr/tutorial/cissi-16/ngrok.png) 

Check that you get the full url in the console when your browser goes to:

    http://XXXXXXXX.ngrok.io/myfile.php?a=65

### Step 4 - Change hostnames in the malware

The malware use the domain 14243444.com to disclose some information. We need to force the malware to use our web server, i.e. our ngrok domain. If you have control over your DNS server, you can easily tune this. Under Linux, the file */etc/hosts* should help you. If you cannot, you can use the following procedure.

Use the [Soot-substitute-hostname tool](tools/) (with Java 8 and do not forget to create the directory lib and put [android.jar inside](tools/lib)) to replace 14243444.com by XXXXXXXX.ngrok.io:

  ```bash
    java -jar ./Soot-substitute-hostname.jar 14243444.com XXXXXXXX.ngrok.io  KitchenTimerReceiver 2fbc32387f9b5c5a8678af3a76c0630ba4d04fd520b21782642a517794063f05.apk
   ```
   	
It will generate a new malware in the sootOutput folder.
	
Sign the malware:

  ```bash
    # If necessary, generate a debug keystore ~/.android/debug.keystore:
    keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000
    # Sign the APK:
    jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore  ~/.android/debug.keystore Kitchen_Service.apk androiddebugkey -storepass android -keypass android
   ```
   
### Step 5 - Launch the malware and see requests !

Connect your smartphone to the internet. Launch the malware. You should capture the leaks sent by the malware !

## Conclusion

The capture of the HTTP traffic enables to see the parameters of the requests. It could also be used to answer these requests, for example if the malware is waiting for commands. Using ngrok enables to bypass any network configuration (but you still need internet). For dangerous or unknown malware, **you should not do this**, because the malware have full access to the internet ! Thus, it would be more safe to implemet a controlled DNS server that will filter the requests and stay confined in a local network.

