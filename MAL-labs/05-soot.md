# Exercise 5: Operate a malware as easy as ABC

![JFL](images/dos2.jpg) 

# Malware:

**Koler**:

- SHA 256: 4d3de2103f740345aa2041691fde0878d7d32e9e4985adf6b030d2e679560118
- get if from [Koodous](https://koodous.com/apks/4d3de2103f740345aa2041691fde0878d7d32e9e4985adf6b030d2e679560118)
## Prerequisite

For this exercise, any smartphone or emulator should work.

You do not need to be inside a virtual machine to perform this exercice. You can use directly Eclipse on your host.

## Introduction

In this exercise, we use Soot, a library that is able to manipulate java classes. In 2013, authors of soot implemented the opening of .apk files. Thus, it easy as ABC to take a malware and automate transformation of its bytecode. This exercise is based on the publication [Instrumenting Android and Java Applications as Easy as abc](https://www.informatik.tu-darmstadt.de/fileadmin/user_upload/Group_EC-Spride/Publikationen/Instrumenting_Android_and_Java_Applications_as_Easy_as_abc.pdf) and on our experience when designing GroddDroid in the publication [GroddDroid: a Gorilla for Triggering Malicious Behaviors](https://hal.inria.fr/hal-01201743).

The studied malware is the **Koler** which is a **spyware**.

The global goal is to log the dynamic information manipulated by the malware by modifying the malware from the inside...

### Step 1 - Testing soot transformations

In this step, we will setup and expore the demo project of Soot (that have been setup for another tutorial in 2013). The project takes an APK RV2013.apk and applies some transformations where SMS are used.

- Clone the repository https://github.com/secure-software-engineering/android-instrumentation-tutorial
- Go to /android-instrumentation-tutorial/instrumentation/manual-instrumentation/RV2013_Soot_instrumentation/RV2013. You'll find the source of the project.
- Import the project into Eclipse. Remove soot from the project Settings > Java Build path. Add lib/soot.jar as a library.
- Remove sootOutput/RV2013.apk.
- Run the main
- Compare apk/RV2013.apk and sootOutput/RV2013.apk. What do you see ?
- Browse MyBodyTransformer to see how to do this ! 

Remark: the source of the application RV2013 is located at android-instrumentation-tutorial/app-example/RV2013. You can rebuilt it if you need to change the source code of RV2013.

### Step 2 - Update soot

Replace soot.jar by a an more [up-to-date version](http://kharon.gforge.inria.fr/tutorial/cyber-berry-17/soot.jar). This will avoid some old bugs coming from the 2013 version of soot.

### Step 3 - Inspect Koler

Quickly inspect the malware in order to find interesting variables or methods that would be interesting to modify.

### Step 4 - Check if an activity is running

In this step we propose to check if the LockActivity is running. We can suspect such an activity to be the one that lock the screen. In order to check this, we will insert some code in the onCreate() method.

- In your transformation body, check if the classname starts with "com.android.LockActivity" and check that the current method contains onCreate. Put a System.out line "Here is LockActivity.onCreate" the  if these conditions are met. Generate Koler.apk and check in the Eclipse output that you see the log "Here is LockActivity.onCreate" (because at some points, soot should encounter this method).
- In this block, you can then iterate over all statements by keeping the code:

      // Here we are in LockActivity.onCreate
      Iterator<Unit> i = body.getUnits().snapshotIterator();
	  while (i.hasNext()) {
	  Unit u = i.next();
	  ...
	  }

- Take inspiration from the replaceStatementByLog method and generate a Log.i call to print "coucou" in the Logcat log, each time a Unit u contains "StrictMode" (for example). This way, when the method onCreate will execute, some "coucou" will appear in the Logcat.
- Generate Koler.apk again. You can check the result in BytecodeViewer. If you are lucky, the decompiler will work. In my case, it crashes. At list, you can look at the Smali code.
- Sign the APK:

    jarsigner -verbose -keystore ~/.android/debug.keystore -storepass android -keypass android Koler.apk androiddebugkey

- Execute it on the smartphone. You are now sure that this Activity is executed.

### Step 4 - Disable the Locker !

:::warning
Difficult !
:::

The goal here is to disable the repetitive task by removing some parts of the code that triggers the lock screen. Some advices:

- you can inspect if a unit is an instance of a soot class, e.g.: (u instanceof InvokeStmt)
- print your statements: System.out.println("My statement:" + u);

## Conclusion

The most difficult part is to generate new code with Soot and to avoid breaking the semantics of the bytecode. Logging is very easy but manipulating variables or creating new ones need more skills :)
