# Exercise 1: Programming an antidote for a ransomware

![Shake](images/shake.png)

## Prerequisite

### Smartphones

For this exercise, you should have one of the following smartphones:

- An emulator with Android 4.1 Jelly Bean with a Nexus S emulated hardware
- Real Nexus S
- Samsung Galaxy SIII


### Tools

- [Jadx](https://github.com/skylot/jadx/releases)
- [Apktool](https://github.com/iBotPeaches/Apktool/releases)
- OpenJDK version 11 or less
- [ADB](https://developer.android.com/tools/adb?hl=fr) (Android Debug Bridge, available in the 'platform-tools' package of the Android SDK)

## Malware

**SimpLocker**:

- SHA 256: 8a918c3aa53ccd89aaa102a235def5dcffa047e75097c1ded2dd2363bae7cf97
- get it from [Koodous](https://koodous.com/apks/8a918c3aa53ccd89aaa102a235def5dcffa047e75097c1ded2dd2363bae7cf97)

## Introduction

In this exercise of the tutorial, we will study the **SimpLocker malware** which is a **ransomware**. It **encrypts the user's files** and displays a fullscreen warning asking to pay a ransom to decrypt the file. The malware is connected to the attacker via the Tor network but we do not need to let the malware connect to Tor.

The goal of this exercise is to understand how works the malware by reverse engineering its code. We will let the malware operate and encrypt your files... But then, we will write **an antidote** by modifying the malware to **force it to decrypt the encrypted files** !

## Let's go !

### Step 1: connect your device

You should see your device by doing:

  ```bash
  adb devices
  List of devices attached
  XXXXX	device
  ```
### Step 2: disconnect the phone from the internet

For this exercise we do not need an internet connection. Disconnect your smartphone from the internet.

### Step 3: take a photo of yourself

We need to provide some personal data to the malware. Take a photo of yourself or something if you do not trust totally this tutorial...

Check in the gallery app that the photo is there.

### Step 4: execute the SimpLocker malware

Install the malware (if the device asks you to share data with google, decline):

  ```bash
  adb install simplelocker.apk
  ```

**Launch the malware** by hitting the icon "Sex xonix" or "Locker" (depends of the phone language). You should see a fullscreen message.

Wait a moment (the malware encrypts your file). You can check the files to see if the malware produces .enc files:

  ```bash
  # Launch a shell in the smartphone/emulator
  adb shell 
  # Go the folder where images are stored:
  ls /sdcard/DCIM/Camera/
  IMG_20250211_125732.jpg.enc
  IMG_20250211_125733.jpg.enc
  ...
  ```

Uninstall the malware:

  ```bash
  adb uninstall org.simplelocker
  ```

Reboot the phone.

  ```bash
  adb reboot
  ```

Check the images of the gallery: you should not see your photo anymore. **All your data is gone (encrypted) !!! Damn !!!**

### Step 5: reverse a simple .apk

We propose to use Jadx which enables to open and decompile an Android application. 

Test Jadx on a demo file [demo1.apk](tools/demo1.apk) that is a small application of three activities (three screens):

  ```bash
  jadx demo1.apk

  # or 

  jadx-gui demo1.apk
  ```

Open the *jf.andro.malcon15demo* package and the *MainActivity* class. You should see a decompilation of the bytecode where two buttons are created in the activity. 

This shows how it is **simple** to browse the source code of an app.

### Step 6: reverse the SimpleLocker malware .apk

Reverse the malware bytecode::

  ```bash
  jadx simplelocker.apk
  # or:
  jadx-gui simplelocker.apk
  ```

**Boom !** You obtain an exception `java.util.zip.ZipException: invalid CEN header (bad entry name or comment)`. This is an obvious protection of the malware developer to prevent any static analysis from classical tools (here, any tools based on the standard Java Zip library, which is most of android analysis tools). Thus, we will need to do the reverse with manual operations.

Unpack the malware:

  ```bash
  mkdir malware-source
  cd malware-source
  unzip ../simplelocker.apk
  Archive:  ../simplelocker.apk
    inflating: res/drawable/btn_green.9.png
    inflating: res/drawable/btn_green_pressed.9.png
    inflating: res/drawable/green_button_background.xml
    inflating: res/drawable/ic_launcher.png
    inflating: res/layout/main_activity.xml
    inflating: res/raw/debiancacerts.bks
    inflating: res/raw/geoip.mp3
    inflating: res/raw/geoip6.mp3
    inflating: res/raw/privoxy_config
    inflating: res/raw/torrc
    inflating: res/raw/torrctether
    inflating: res/xml/policies.xml
    inflating: AndroidManifest.xml
    inflating: resources.arsc
    inflating: res/drawable-hdpi/ic_launcher.png
    inflating: res/drawable-hdpi/ic_menu_key.png
    inflating: res/drawable-ldpi/ic_launcher.png
    inflating: res/drawable-ldpi/ic_menu_key.png
    inflating: res/drawable-mdpi/ic_launcher.png
    inflating: res/drawable-mdpi/ic_menu_key.png
    inflating: res/drawable-xhdpi/ic_launcher.png
    inflating: classes.dex
    inflating: lib/armeabi/libobfsproxy.so
    inflating: lib/armeabi/libprivoxy.so
    inflating: lib/armeabi/libtor.so
    inflating: lib/armeabi/libxtables.so
    inflating: META-INF/MANIFEST.MF
    inflating: META-INF/CERT.SF
    inflating: META-INF/CERT.RSA
    inflating: META-INF/screen3.jpg
    inflating: META-INF/????????? (2).jpg
    inflating: META-INF/1372587162_chto-takoe-root-prava.jpg
    inflating: META-INF/????????? (1).jpg
  cd ..
  ```

You can see some strange files in `META-INF/`. This generated in the process or creating a jar file and contains meta data about the archive (not used by android), and the signatures of the files in the archive (used by Android as its first signature scheme. Later signature versions store signature in a non standard zip section just after the zip Central Directory or in file separated from the application). There is no version to have images in this folder.

Thankfully, we only need `META-INF/` to check the signature of the application. This means we can just remove the folder from the application altogether: 

  ```
  zip -d simplelocker.apk 'META-INF/*'
  ```

Now you can browse the code of SimpleLocker.

  ```
  jadx simplelocker.apk
  ```

The malware developer is a well organized person. All the malicious code is located in the org.simplocker package. Have a look to:

- **FilesEncryptor**: contains the methods that iterate over files and *encrypt()* or *decrypt()* them. 
- **AesCrypt**: contains the encryption implementation. The constructor parameter is the encryption key.

Search all classes that call "encrypt()". 

  ```
  grep -rn 'encrypt()' simplelocker/sources
  ```

You should find that the *MainService* class launches the encryption. Of course, the ciphering process is performed in background. We can suppose that the deciphering is also performed in a service.

Now, search the calls to *decrypt()*. You will discover that the call is handled in **HttpSender**. Of course, the malware waits for a message from the attacker's server to decipher the files. 

Modifying this part is not an easy task (but would be possible). A better idea is to modify the primary behavior of the malware when *MainService* encrypts files. **Instead of calling the *encrypt()* method, we will force *MainService* to call the *decrypt()* method**. This way, our new malware will act as an antidote !

### Step 7: edit the classes and repackage

Recompiling an application from the Java code returned by Jadx is almost impossible, they are always parts of the code that are approximated/that failed to decompiled. Instead, we will work on the smali representation of the bytecode (an assembly-like langage for android bytecode). 

First, you need to depackage the application and disassemble the bytecode stored in `classes.dex`. Apktool can do that for you now that we removed the invalid files in `META-INF/`:

  ```
  apktool d simplelocker.apk -o simplelocker_depacked
  ```

You will noticed that `encrypt` is not called from `simplelocker_depacked/smali/org/simplelocker/MainService.smali`. Indeed, if you look at the java code, you will see that the method is called by an anonymous class that implement the Runnable interface:

  ```java
  new Thread(new Runnable() { // from class: org.simplelocker.MainService.5
      @Override // java.lang.Runnable
      public void run() {
          try {
              FilesEncryptor encryptor = new FilesEncryptor(MainService.this.context);
              encryptor.encrypt();
          } catch (Exception e) {
              Log.d(Constants.DEBUG_TAG, "Error: " + e.getMessage());
          }
      }
  }).start();
  ```

The actual class we are looking for is in fact `Lorg/simplelocker/MainService$5` (You can also find it by grepping for `Lorg/simplelocker/FilesEncryptor;->encrypt()V`: `grep -rn 'Lorg/simplelocker/FilesEncryptor;->encrypt()V' simplelocker_depacked/`).

Change the *encrypt()* to a call to *decrypt()*. Save the file.

Now repackage the apk, once again, Apktool can do that for you:

  ```
  apktool b simplelocker_depacked -o simplelocker-antidote.unaligned.apk
  ```

Now you need to align and sign the application. To do that, you can use android an java tools. First, align the application with `zipalign` (from an android SDK package `build-tools;v.e.r`, like `build-tools;34.0.0`):

  ```
  zipalign -v -f 4 simplelocker-antidote.unaligned.apk simplelocker-antidote.unsigned.apk
  ```

Be careful ! You will **need the version 11 of open JDK** to get the jarsigner version that accepts to make a signature MD5withRSA or SHA1. In later versions of Open JDK, these algorithms have been deprecated and jarsigner is doing RSA signatures. The emulator will fail with RSA signatures.

Check the version of jarsigner:

```bash
jf@mascotte:~/Téléchargements$ which jarsigner
/usr/bin/jarsigner
jf@mascotte:~/Téléchargements$ ls -l /usr/bin/jarsigner
lrwxrwxrwx 1 root root 27 janv. 11 15:31 /usr/bin/jarsigner -> /etc/alternatives/jarsigner
jf@mascotte:~/Téléchargements$ ls -l /etc/alternatives/jarsigner
lrwxrwxrwx 1 root root 48 janv. 11 15:31 /etc/alternatives/jarsigner -> /usr/lib/jvm/java-11-openjdk-amd64/bin/jarsigner
```

When you are sure to have jarsigner from the version 11 of the JDK, you can rebuild your apk and sign it:

  ```bash
  # Generate a debug keystore:
  keytool -genkey -v -dname "CN=SomeKey,O=SomeOne,C=FR" -keystore lab.keystore -storepass android -alias SignKey -keypass android -keyalg RSA -keysize 2048 -validity 10000
  jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore lab.keystore -storepass android -keypass android -signedjar simplelocker-antidote.apk simplelocker-antidote.unsigned.apk SignKey
  ```
  
We have now the **antidote** against SimpLocker :smile:

### Step 8: deploy the antidote

Install the **antidote** (if the device asks you to share data with google, decline):

  ```bash
  adb install simplelocker-antidote.apk
  ```

**Launch the malware** by hitting the icon "Sex xonix" or "Locker". You should see a fullscreen message.

Wait a moment (the antidote deciphers your file). You can check the files to see if the malware deciphers .enc files:

  ```bash
  adb shell ls /sdcard/DCIM/Camera
  IMG_20250213_101508.jpg
  IMG_20250213_101509.jpg
  ...
  ```

Eventually reboot the phone. Uninstall `adb uninstall org.simplelocker`, open the gallery app. Your photos should **reappear** magically ! 

## Conclusion

This malware is a fully functional ransomware that is able to decipher the encrypted files. The command is sent via the Tor network by the attacker, when the user has paid for recovering its files. The main drawback of the malware is the poor quality of the ciphering algorithm that uses a constant as encryption key. A more advanced version of such a malware should generate the key and store it on server side to become a more dangerous malware.

For more information, you can enjoy the Kharon webpage about this malware. The [system flow graph representation](https://cidre.gitlabpages.inria.fr/malware/malware-website/dataset/SimpLocker_sample_fd694cf5ca1dd4967ad6e8c67241114c.html) gives you a view of the Tor process used to anonymize the communication with the attacker. The play button at the end of the pages, replays the interaction that occured between the processes.

